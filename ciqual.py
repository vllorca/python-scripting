#!/usr/bin/env python

import sqlite3 
import csv 

# DB creation
conn = sqlite3.connect('ciqual.db')
cur = conn.cursor()

# Tables creation
try :
    cur.execute('''CREATE TABLE nutrient (
                    id INTEGER PRIMARY KEY AUTOINCREMENT, 
                    name text)''')

    cur.execute('''CREATE TABLE food (
                    id text PRIMARY KEY, 
                    name text, 
                    grp_id text, 
                    ssgrp_id text, 
                    ssssgrp_id text,
                    foreign key (grp_id) references Grp (id),
                    foreign key (ssgrp_id) references SSGrp (id),
                    foreign key (ssssgrp_id) references SSSSGrp (id))''')

    cur.execute('''CREATE TABLE 'group' (
                    id text PRIMARY KEY UNIQUE, 
                    name text)''')

    cur.execute('''CREATE TABLE 'ssgroup' (
                    id text PRIMARY KEY UNIQUE,  
                    name text,
                    grp_id text,
                    foreign key (grp_id) references 'group' (id))''')

    cur.execute('''CREATE TABLE 'ssssgroup' (
                    id text PRIMARY KEY UNIQUE, 
                    name text,
                    ssgrp_id,
                    foreign key (ssgrp_id) references SSGrp (id))''')

    cur.execute('''CREATE TABLE nutdata (
                    id INTEGER PRIMARY KEY AUTOINCREMENT, 
                    food_id text, 
                    nutrient_id integer,
                    value text,
                    foreign key (nutrient_id) references nutrient (id),
                    foreign key (food_id) references food (id)
                    )''')

except sqlite3.OperationalError :
    pass

nutrientSQLTemplate     = '''INSERT INTO nutrient (name) VALUES (?);'''
foodSQLTemplate         = '''INSERT INTO food (id, name, grp_id, ssgrp_id, ssssgrp_id) VALUES (?,?,?,?,?);'''
groupSQLTemplate        = '''INSERT INTO 'group' (id, name) VALUES (?,?);'''
ssgroupSQLTemplate      = '''INSERT INTO 'ssgroup' (id, name, grp_id) VALUES (?,?,?);'''
ssssgroupSQLTemplate    = '''INSERT INTO 'ssssgroup' (id, name, ssgrp_id) VALUES (?,?,?);'''
nutdataSQLTemplate      = '''INSERT INTO nutdata (food_id, nutrient_id, value) VALUES (?,?,?);'''

with open('table_ciqual.csv') as csvfile:
    reader = csv.DictReader(csvfile)

    nutDict = dict()
    nutDict_id = 1

    header_dict=next(reader)

    for index, key in enumerate(header_dict):
        if index >= 8:
            cur.execute(nutrientSQLTemplate, (key,))
            nutDict[nutDict_id] = key
            nutDict_id += 1

        memory_grp_code = "00"
        memory_ssgrp_code = "0000"
        memory_ssssgrp_code = "000000"

    for row in reader:  
        grp_code = row['alim_grp_code']
        ssgrp_code = row['alim_ssgrp_code']
        ssssgrp_code = row['alim_ssssgrp_code']

        data_food_list=[row['alim_code'], row['alim_nom_fr'],row['alim_grp_code'], row['alim_ssgrp_code'], row['alim_ssssgrp_code']]
        data_group_list=[row['alim_grp_code'], row['alim_grp_nom_fr']]
        data_ssgroup_list=[row['alim_ssgrp_code'], row['alim_ssgrp_nom_fr'], row['alim_grp_code']]
        data_ssssgroup_list=[row['alim_ssssgrp_code'], row['alim_ssssgrp_nom_fr'], row['alim_ssgrp_code']]

        for nutri in nutDict:
            data_nutdata_list=[row['alim_code'], nutDict[nutri], row[nutDict[nutri]]]
           
        try :
            if grp_code != memory_grp_code :
                cur.execute(groupSQLTemplate, data_group_list)

            if ssgrp_code != memory_ssgrp_code :
                cur.execute(ssgroupSQLTemplate, data_ssgroup_list)

            if ssssgrp_code != memory_ssssgrp_code :
                cur.execute(ssssgroupSQLTemplate, data_ssssgroup_list)
            
            cur.execute(foodSQLTemplate, data_food_list)
            cur.execute(nutdataSQLTemplate, data_nutdata_list)

            memory_grp_code = grp_code
            memory_ssgrp_code = ssgrp_code
            memory_ssssgrp_code = ssssgrp_code
        
        except :
            pass
            
conn.commit() 
conn.close() 

