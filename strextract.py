#!/usr/bin/env python

import argparse
import os, sys, re

parser = argparse.ArgumentParser()
parser.add_argument("directoryPath", type=str, help="Give a directory Path")
parser.add_argument("-suffix", type=str, help="limit search with a suffix", default='')
parser.add_argument("-path", help="Print directory path", action="store_true")
parser.add_argument("-all", "-a", help="Show hidden files", action="store_true")
args = parser.parse_args()

def strextract() :
    for root, dirs, files in os.walk(args.directoryPath):
        pattern = re.compile("\'(.+?)\'|\"(.+?)\"")

        try :
            for filename in files:
                if args.all or not filename.startswith('.'):
                    if not args.suffix or filename.endswith(args.suffix):
                        
                        file = open(os.path.join(root, filename), 'r') 
                        for line in file :
                            for match in pattern.finditer(line):
                                string = (os.path.join(root, filename), match)
                                if args.path :
                                    print('{} > {}'.format(os.path.join(root, filename), match.group(0)))   
                                else :
                                    print(match.group(0))                                            
                                          
        except :
            print(f'An error has been encountered with the file {os.path.join(root, filename)}.')

     
    
                    
def main():
    strextract()

if __name__ == '__main__':
    main()