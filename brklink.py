#!/usr/bin/env python3

from bs4 import BeautifulSoup, SoupStrainer
from requests.exceptions import MissingSchema
from urllib.parse import urljoin

import requests
import argparse

visited_links = []

parser = argparse.ArgumentParser()
parser.add_argument("-n", type=int, help="depth to searh for", default=1)
parser.add_argument("url", type=str, help="Site's url to check broken links")
args = parser.parse_args()

def check_links(url, depth) :

    if depth >= 0 :
        if requests.get(url).status_code >= 400 :
            print(url)
            pass

        visited_links.append(url)
        page = requests.get(url)
        data = page.text
        soup = BeautifulSoup(data, 'lxml')
        
        for link in soup.find_all('a'):
            full_url_link = urljoin(url, link.get('href'))
            if full_url_link not in visited_links :
                check_links(full_url_link, depth - 1)

def main():
    check_links(args.url, args.n)


if __name__ == '__main__':
    main()